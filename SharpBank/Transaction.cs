﻿using System;

namespace SharpBank
{
    public class Transaction
    {
        public readonly double amount;

        private DateTime transactionDate;

        public Transaction(double amount)
        {
            this.amount = amount;
            this.TransactionDate = DateProvider.GetInstance().Now();
        }

        public DateTime TransactionDate { get => transactionDate; set => transactionDate = value; }
    }
}
