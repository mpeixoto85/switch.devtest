﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank
{
    public class Account
    {

        public const int CHECKING = 0;
        public const int SAVINGS = 1;
        public const int MAXI_SAVINGS = 2;

        private readonly int accountType;
        public List<Transaction> transactions;
        private double amount;

        public double Amount { get => amount; set => amount = value; }

        public Account(int accountType)
        {
            this.accountType = accountType;
            this.transactions = new List<Transaction>();
        }

        public void Deposit(double amount)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("amount must be greater than zero");
            }
            else
            {
                transactions.Add(new Transaction(amount));
                this.amount += amount;
            }
        }

        public void Withdraw(double amount)
        {
            if (amount <= 0 )
            {
                throw new ArgumentException("amount must be greater than zero");
            }
            else if (amount >= this.Amount)
            {
                throw new ArgumentException("you haven't enough funds in your account");
            }
            else
            {
                transactions.Add(new Transaction(-amount));
                this.amount += -amount;
            }
        }

        public double InterestEarned()
        {
            double amount = SumTransactions();
            switch (accountType)
            {
                case SAVINGS:
                    if (amount <= 1000)
                        return amount * 0.001;
                    else
                        return 1 + (amount - 1000) * 0.002;
                case MAXI_SAVINGS:
                    if (CheckIfWithdrawalExist(10))
                        return amount * 0.001;
                    else
                        return amount  * 0.05;
                default:
                    return amount * 0.001;
            }
        }

        public double SumTransactions()
        {
            double amount=0;
            foreach (Transaction t in transactions)
            {
                amount += t.amount;
            }
            return amount;
        }

        private bool CheckIfWithdrawalExist(int days)
        {

            while(days>=0)
            { 
                foreach (Transaction t in transactions)
                { 
                    if (t.TransactionDate.Date == (DateTime.Today.AddDays(-days).Date))
                        if (t.amount < 0)
                            return true;
                }
                days--;
            }
            return false;
        }

        public int GetAccountType()
        {
            return accountType;
        }

    }
}
