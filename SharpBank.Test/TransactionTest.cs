﻿using NUnit.Framework;

namespace SharpBank.Test
{
    [TestFixture]
    public class TransactionTest
    {
        [Test]
        public void Transaction()
        {
            Transaction t = new Transaction(5);
            Assert.AreEqual(t.TransactionDate.Date, DateProvider.GetInstance().Now());
            Assert.AreEqual(t.amount, 5);
        }
    }
}
